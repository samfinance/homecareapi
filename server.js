var express = require("express");  
var path = require("path");  
var mongo = require("mongoose");   
var bodyParser = require('body-parser');   
var morgan = require("morgan");  
var db = require("./config.js");  
 var multer =require('multer');
  /*var storage = multer.diskStorage({
    destination:function(req,file,callback){
      callback(null, './uploads');
    },
    filename:function(req,file,callback){
      callback(null, file.fieldname + '-' + Date.now());
    }
  })
 var upload=multer({storage:storage}).single('image'); */
var app = express();  
var port = process.env.port || 7777;  
var srcpath  =path.join(__dirname,'/public') ;  
app.use(express.static('public'));  
app.use(bodyParser.json({limit:'5mb'}));    
app.use(bodyParser.urlencoded({extended:true, limit:'5mb'}));  
 
global.__basedir = __dirname;
  
var mongoose = require('mongoose');  
var Schema = mongoose.Schema;  
/*var studentSchema = new Schema({      
    name: { type: String   },       
    address: { type: String   },     
    email: { type: String },       
    contact: { type: String },       
},{ versionKey: false });  */

var serviceSchema = new Schema({      
  name: { type: String   },       
  address: { type: String   },     
  image: { type: String },       
  price: { type: String },       
  call: { type: String },
  category: { type: String },       
},{ versionKey: false }); 

var userSchema = new Schema({      
  Fname: { type: String   },       
  phoneNo: { type: String   },     
  address: { type: String },       
  email: { type: String },       
  gender: { type: String },       
  nationality: { type: String },       
  password: { type: String },
  status: { type: String },     
},{ versionKey: false });  

var appointmentSchema = new Schema({      
  location: { type: String   },       
  date: { type: String   },     
  time: { type: String },       
  name: { type: String },
  service: { type: String }, 
  image: { type: String },        
  phone: { type: String },       
  UserId: { type: String },     
},{ versionKey: false });

var headerSchema = new Schema({      
  header: { type: String   },       
  description: { type: String   },    
},{ versionKey: false });
   
const fileWorker = require('./app/controllers/file.controller.js');
var upload = require('./app/config/multer.config.js');

var model = mongoose.model('service', serviceSchema, 'service');  
var modeluser = mongoose.model('user', userSchema, 'user'); 
var modelheader = mongoose.model('Head', headerSchema, 'Head'); 
var modelappointment = mongoose.model('appointment', appointmentSchema, 'appointment'); 

app.get('/api/users', function (req, res) {
    let user=[
      {
        "id": 1,
        "name": "Leanne Graham",
        "username": "Bret",
        "email": "Sincere@april.biz",
        "address": {
          "street": "Kulas Light",
          "suite": "Apt. 556",
          "city": "Gwenborough",
          "zipcode": "92998-3874",
          "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
          }
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
          "name": "Romaguera-Crona",
          "catchPhrase": "Multi-layered client-server neural-net",
          "bs": "harness real-time e-markets"
        }
      },
      {
        "id": 2,
        "name": "Ervin Howell",
        "username": "Antonette",
        "email": "Shanna@melissa.tv",
        "address": {
          "street": "Victor Plains",
          "suite": "Suite 879",
          "city": "Wisokyburgh",
          "zipcode": "90566-7771",
          "geo": {
            "lat": "-43.9509",
            "lng": "-34.4618"
          }
        },
        "phone": "010-692-6593 x09125",
        "website": "anastasia.net",
        "company": {
          "name": "Deckow-Crist",
          "catchPhrase": "Proactive didactic contingency",
          "bs": "synergize scalable supply-chains"
        }
      }];
      res.setHeader('Access-Control-Expose-Headers', 'Content-Range');
          res.setHeader('Content-Range', 5);
         // res.send({ data: categories, total: 5 });
    res.send({data:user})
  })
  
//api for get data from database  
app.get("/api/getdata",function(req,res){   
 model.find({},function(err,data){

	res.set('Access-Control-Allow-Origin','*');  
            if(err){  
                res.send(err);  
            }  
            else{             
                res.send(data);  
                }  
        });  
}) 

//api for get user from database  
app.get("/api/getuser",function(req,res){   
  modeluser.find({},function(err,data){

	res.set('Access-Control-Allow-Origin','*');  
            if(err){  
                res.send(err);  
            }  
            else{             
                res.send(data);  
                }  
        });  
}) 

//api for get header from database  
app.get("/api/getheader",function(req,res){   
  modelheader.find({},function(err,data){

	res.set('Access-Control-Allow-Origin','*');  
            if(err){  
                res.send(err);  
            }  
            else{             
                res.send(data);  
                }  
        });  
}) 

  
//api for check user existing from database  
app.post("/api/serviceCheck",function(req,res){   
  model.find({category:req.body.category},   
 function(err,data) {  
  if (err) {  
  res.send(err);  
  
  } 
  else if(data.length > 0){
   res.send(data);  
  }
  else{
    res.send("no service");
  }
  });  
 }) 

 //api for admin login existing from database  
app.post("/api/adminlogin",function(req,res){  
  res.set('Access-Control-Allow-Origin','*');  
  modeluser.find({email:req.body.email,password:req.body.password,status:req.body.status},   
 function(err,data) {  
  if (err) {  
  res.send(err);  
  
  } 
  else if(data.length > 0){
   res.send(data);  
  }
  else{
    res.send({data:"no user"});
  }
  });  
 }) 

 //api for user login existing from database  
app.post("/api/userlogin",function(req,res){  
  res.set('Access-Control-Allow-Origin','*');  
  modeluser.find({phoneNo:req.body.phoneNo,password:req.body.password,status:req.body.status},{password:0},   
 function(err,data) {  
  if (err) {  
  res.send({data:err});  
  
  } 
  else if(data.length > 0){
   res.send({data:data});  
  }
  else{
    res.send({data:"no user"});
  }
  });  
 }) 

//api for user appointment from database  
app.post("/api/userappointment",function(req,res){  
  res.set('Access-Control-Allow-Origin','*');  
  console.log("data: ",req.body);
  modelappointment.find({UserId:req.body.UserId},   
 function(err,data) {  
  if (err) {  
  res.send({data:err});  
  
  } 
  else if(data.length > 0){
   res.send({data:data});  
  }
  else{
    res.send({data:"no appointment"});
  }
  });  
 }) 


//api for  appointment from database  
app.get("/api/appointment",function(req,res){  
  res.set('Access-Control-Allow-Origin','*');  
  //console.log("data: ",req.body);
  modelappointment.find({},   
 function(err,data) {  
  if (err) {  
  res.send({data:err});  
  
  } 
  else if(data.length > 0){
   res.send({data:data});  
  }
  else{
    res.send({data:"no appointment"});
  }
  });  
 }) 
  
  
//api for Delete data from database  deleteOne
app.post("/api/Removedata",function(req,res){   
// model.remove({ _id: req.body.id }, function(err) {
	console.log("data: ",req.body.id);
 model.deleteOne({ _id: req.body.id }, function(err) { 
	res.set('Access-Control-Allow-Origin','*');  
	
            if(err){  
                res.send(err);  
            }  
            else{    
                   res.send({data:"Service has been Deleted..!!"});             
               }  
        });  
}) 


//api for Delete user from database  deleteOne
app.post("/api/Removeuser",function(req,res){   
// model.remove({ _id: req.body.id }, function(err) {
	console.log("data: ",req.body.id);
 modeluser.deleteOne({ _id: req.body.id }, function(err) { 
	res.set('Access-Control-Allow-Origin','*');  
	
            if(err){  
                res.send(err);  
            }  
            else{    
                   res.send({data:"User has been Deleted..!!"});             
               }  
        });  
})  

//api for Delete Appointment from database  deleteOne
app.post("/api/Removedappointment",function(req,res){   
// model.remove({ _id: req.body.id }, function(err) {
	console.log("data: ",req.body.id);
 modelappointment.deleteOne({ _id: req.body.id }, function(err) { 
	res.set('Access-Control-Allow-Origin','*');  
	
            if(err){  
                res.send(err);  
            }  
            else{    
                   res.send({data:"Appointment has been Deleted..!!"});             
               }  
        });  
}) 

 
  
/*app.post('/api/upload',upload.single('image'),function(req,res){
  console.log(req.file,req.body)
})*/
app.post('/api/files/upload', upload.single("image"), fileWorker.uploadFile);

app.get('/api/files/getall', fileWorker.listAllFiles);

app.get('/api/files/:filename', fileWorker.readFiles);

app.post('/api/photo',function(req,res){
  upload(req,res,function(err){
    if(err){
      return res.send("error uploading file");
    }
    res.send("file is uploaded");
  })
})
  
//api for Update data from database  
app.post("/api/Updatedata",function(req,res){  
res.set('Access-Control-Allow-Origin','*');  
 model.findByIdAndUpdate(req.body.id, { name:  req.body.name, image: req.body.image, price: req.body.price,call:req.body.call,category:req.body.category },   
function(err) {  
 if (err) {  
 res.send(err);  
 return;  
 }  
 res.send({data:"Record has been Updated..!!"});  
 });  
}) 

//api for Update header from database  
app.post("/api/Updateheader",function(req,res){  
res.set('Access-Control-Allow-Origin','*');  
 modelheader.findByIdAndUpdate(req.body.id, { header:  req.body.header, description: req.body.description },   
function(err) {  
 if (err) {  
 res.send(err);  
 return;  
 }  
 res.send({data:"Header has been Updated..!!"});  
 });  
})    
  
  
//api for Insert data from database  
app.post("/api/savedata",function(req,res){   
  res.set('Access-Control-Allow-Origin','*'); 
  console.log("data: ",req.body);
    var mod = new model(req.body);  
    
        mod.save(function(err,data){  
            if(err){  
                res.send(err);                
            }  
            else{        
                 res.send({data:"Record has been Inserted..!!"});  
            }  
        });  
}) 

//api for Insert header from database  
app.post("/api/saveheader",function(req,res){   
  res.set('Access-Control-Allow-Origin','*'); 
  console.log("data: ",req.body);
    var mod = new modelheader(req.body);  
    
        mod.save(function(err,data){  
            if(err){  
                res.send(err);                
            }  
            else{        
                 res.send({data:"Header has been Inserted..!!"});  
            }  
        });  
})  

//api for Insert user data from database  
app.post("/api/saveuserdata",function(req,res){   
  res.set('Access-Control-Allow-Origin','*'); 
  console.log("data: ",req.body);
    var mod = new modeluser(req.body);  
    
        mod.save(function(err,data){  
            if(err){  
                res.send({data:err});                
            }  
            else{        
                 res.send({data:"Record has been Inserted..!!"});  
            }  
        });  
})

//api for Insert appointment data from database  
app.post("/api/saveappointment",function(req,res){   
  res.set('Access-Control-Allow-Origin','*'); 
  console.log("data: ",req.body);
    var mod = new modelappointment(req.body);  
    
        mod.save(function(err,data){  
            if(err){  
                res.send({data:err});                
            }  
            else{        
                 res.send({data:"Record has been Inserted..!!"});  
            }  
        }); 
})
      
// call by default index.html page  
app.get("*",function(req,res){   
    res.sendFile(srcpath +'/index.html');  
}) 

// call by default index.html page  
app.get("/user",function(req,res){   
  res.sendFile(srcpath +'/user.html');  
})
  
//server stat on given port  
app.listen(port,function(){   
    console.log("server start on port"+ port);  
}) 
