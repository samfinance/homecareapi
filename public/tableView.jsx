//import styles from 'vendor/datatables/dataTables.bootstrap4.min.css';
//import 'css/sb-admin-2.min.css'; 
var StudentAll = React.createClass({   
  
    getInitialState: function () {  
      return { name: '' ,address: '',email:'',contact:'',id:'',Buttontxt:'Save', data1: []};  
    },  
     handleChange: function(e) {  
          this.setState({[e.target.name]: e.target.value});  
      },   
    
    componentDidMount() {  
     
      $.ajax({  
         url: "http://localhost:7777/api/getdata",  
         type: "GET",  
         dataType: 'json',  
         ContentType: 'application/json',  
         success: function(data) {           
           this.setState({data1: data});   
             
         }.bind(this),  
         error: function(jqXHR) {  
           console.log(jqXHR);  
               
         }.bind(this)  
      });  
    },  
      
  DeleteData(id){  
    var studentDelete = {  
          'id': id  
             };        
      $.ajax({  
        url: "http://localhost:7777/api/Removedata/",  
        dataType: 'json',  
        type: 'POST',  
        data: studentDelete,  
        success: function(data) {  
          alert(data.data);  
           this.componentDidMount();  
    
        }.bind(this),  
        error: function(xhr, status, err) {  
           alert(err);   
               
              
        }.bind(this),  
        });  
      },  
     
      EditData(item){           
     this.setState({name: item.name,address:item.address,contact:item.contact,email:item.email,id:item._id,Buttontxt:'Update'});  
       },  
    
     handleClick: function() {  
     
     var Url="";  
     var Url1="http://lunarweb.herokuapp.com/users/register";
     if(this.state.Buttontxt=="Save"){  
        Url="/api/savedata";  
         }  
        else{  
        Url="http://localhost:7777/api/Updatedata";  
        }  
        var studentdata = {  
          'name': this.state.name,  
          'address':this.state.address,  
          'email':this.state.email,  
          'contact':this.state.contact,  
          'id':this.state.id,  
            
        } 

       var maped={
          'name': 'john doe',
          'email': 'john@lunar.com',
          'phone': '0765498768',
          'idnumber': 'bba2345',
          'college': 'bba',
          'course':'bba',
          'gender': 'male',
          'password': 'john'
        } 
      $.ajax({  
        url: Url,  
        dataType: 'json',  
        type: 'POST',  
        data: studentdata,  
        success: function(data) {         
            alert(data.data);         
            this.setState(this.getInitialState());  
            this.componentDidMount();  
             
        }.bind(this),  
        error: function(xhr, status, err) {  
           alert(err);       
        }.bind(this)  
      });  
    },  
    
    render: function() {  
      return (   
        <div className="container">  
          
    <form>  
     
     
  {/*<!-- DataTales Example -->*/}
                    
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    {this.state.data1.map((item, index) => ( 
                                        <tr key={index}>
                                            <td>{index+1}</td>   
                                            <td>{item.name}</td>                        
                                            <td>{item.address}</td>   
                                            <td>{item.email}</td>  
                                            <td>{item.contact}</td> 
                                            <td>   
              
                                          <button type="button" className="btn btn-success" onClick={(e) => {this.EditData(item)}}>Edit</button>      
                                          </td>   
                                          <td>   
                                            <button type="button" className="btn btn-info" onClick={(e) => {this.DeleteData(item._id)}}>Delete</button>  
                                          </td>   
                                        </tr>
                                    ))}  
                                    </tbody>
                                </table>
  
  </form>          
        </div>  
      );  
    }  
  });  
    
  ReactDOM.render(<StudentAll  />, document.getElementById('root')) 